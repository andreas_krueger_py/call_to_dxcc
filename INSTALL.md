# Local build and install:

## To build:

    rm -rf call_to_dxcc.egg-info build dist
    python setup.py sdist bdist_wheel

## To install:

    pip install dist/call_to_dxcc-*-py3-none-any.whl

or else (your choice)

    pip install dist/call_to_dxcc-*.tar.gz



